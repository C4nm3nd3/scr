using NameSpace System.Security.Principal

Write-Host `n"INFORMACIÓN SOBRE ESTE EQUIPO:"

Get-CimInstance -ClassName Win32_Desktop | Select-Object -ExcludeProperty "CIM*" | Format-Table -AutoSize


function get-WmiBiosCharacteristics {
param ([uint16] $bitems)

#---------------------retornara datos si encuentra alguno de los valores en la variable $bitems-----------



If ($bitems -le 39) {

    switch ($bitems) {
    0   {"00-Reserved"}
    1   {"01-Reserved"}
    2   {"02-Unknown"}
    3   {"03-BIOS Characteristics Not Supported"}
    4   {"04-ISA is supported"}
    5   {"05-MCA is supported"}
    6   {"06-EISA is supported"}
    7   {"07-PCI is supported"}
    8   {"08-PC Card (PCMCIA) is supported"}
    9   {"09-Plug and Play is supported"}
    10  {"10-APM is supported"}
    11  {"11-BIOS is Upgradable (Flash)"}
    12  {"12-BIOS shadowing is allowed"}
    13  {"13-VL-VESA is supported"}
    14  {"14-ESCD support is available"}
    15  {"15-Boot from CD is supported"}
    16  {"16-Selectable Boot is supported"}
    17  {"17-BIOS ROM is socketed"}
    18  {"18-Boot From PC Card (PCMCIA) is supported"}
    19  {"19-EDD (Enhanced Disk Drive) Specification is supported"}
    20  {"20-Int 13h - Japanese Floppy for NEC 9800 1.2mb (3.5, 1k Bytes/Sector, 360 RPM) is supported"}
    21  {"21-Int 13h - Japanese Floppy for Toshiba 1.2mb (3.5, 360 RPM) is supported"}
    22  {"22-Int 13h - 5.25 / 360 KB Floppy Services are supported"}
    23  {"23-Int 13h - 5.25 /1.2MB Floppy Services are supported"}
    24  {"24-Int 13h - 3.5 / 720 KB Floppy Services are supported"}
    25  {"25-Int 13h - 3.5 / 2.88 MB Floppy Services are supported"}
    26  {"26-Int 5h, Print Screen Service is supported"}
    27  {"27-Int 9h, 8042 Keyboard services are supported"}
    28  {"28-Int 14h, Serial Services are supported"}
    29  {"29-Int 17h, printer services are supported"}
    30  {"30-Int 10h, CGA/Mono Video Services are supported"}
    31  {"31-NEC PC-98"}
    32  {"32-ACPI supported"}
    33  {"33-USB Legacy is supported"}
    34  {"34-AGP is supported"}
    35  {"35-I2O boot is supported"}
    36  {"36-LS-120 boot is supported"}
    37  {"37-ATAPI ZIP Drive boot is supported"}
    38  {"38-1394 boot is supported"}
    39  {"39-Smart Battery supported"}
        }
    Return
    }

    If ($bitems -ge 40 -and $bitems -le 45) {
          "{0}-Reserved for BIOS vendor" -f $bitems
    return
            }

    If ($bitems -ge 48 -and $bitems -le 63) {
           "{0}-Reserved for system vendor" -f $bitems
    return
            }
"{0}-Unknown Value " -f $bitems
    }

#-----------------------Trae info del bios y lo guarda en $bios-----------------

$bios = Get-WMIObject Win32_Bios

#---------------------- Muestra las caracteristicas del bios--------------------
Write-Host `n"Caracteristicas del BIOS:"
""
foreach ($ch in $bios.BiosCharacteristics) {
"                      :  {0}" -f  (Get-WmiBiosCharacteristics($ch))
} 
"Bios Version          :  {0}" -f $bios.BiosVersion
"Codeset               :  {0}" -f $bios.Codeset
"CurrentLanguage       :  {0}" -f $bios.CurrentLanguage
"Description           :  {0}" -f $bios.Description
"IdentificatonCode     :  {0}" -f $bios.IdentificatonCode
"InstallableLanguages  :  {0}" -f $bios.InstallableLanguages
"InstallDate           :  {0}" -f $bios.InstallDate 
"LanguageEdition       :  {0}" -f $bios.LanguageEdition
"ListOfLanguages       :  {0}" -f $bios.ListOfLanguages
"Manufacturer          :  {0}" -f $bios.Manufacturer
"OtherTargetOS         :  {0}" -f $bios.OtherTargetOS
"PrimaryBIOS           :  {0}" -f $bios.PrimaryBIOS
"ReleaseDate           :  {0}" -f $bios.ReleaseDate
"SerialNumber          :  {0}" -f $bios.SerialNumber
"SMBIOSBIOSVersion     :  {0}" -f $bios.SMBIOSBIOSVersion
"SMBIOSMajorVersion    :  {0}" -f $bios.SMBIOSMajorVersion
"SMBIOSMinorVersion    :  {0}" -f $bios.SMBIOSMinorVersion
"SoftwareElementID     :  {0}" -f $bios.SoftwareElementID 
"SoftwareElementState  :  {0}" -f $bios.SoftwareElementState
"TargetOperatingSystem :  {0}" -f $bios.TargetOperatingSystem
"Version               :  {0}" -f $bios.Version 

##########################################

#comprueba los permisos de ejecucion de scripts, usualmente en RemoteSigned para permitir solo los firmados OK 

$polexec = Get-ExecutionPolicy
if ($polexec -eq "RemoteSigned"){Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser} #pedira confirmación del usuario si se ejecuta como administrador


Write-Host `n"
#############################################
TIENE PERMISO PARA EJECUTAR ESTE SCRIPT....
#############################################"

<#
-------------------------------------------
SELECTOR DE FECHAS
------------------------------------------#>


Write-Host `n"
##################################################
SELECCIONE FECHA DE INICIO Y FINAL PARA AUDITORIA:
##################################################"




Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing

<#----------------Selector fecha inicio-------------------#>

    $form = New-Object Windows.Forms.Form -Property @{
        StartPosition = [Windows.Forms.FormStartPosition]::CenterScreen
        Size          = New-Object Drawing.Size 243, 230
        Text          = 'Seleccione Fecha INICIAL de busqueda'
        Topmost       = $true
        }

    $calendar = New-Object Windows.Forms.MonthCalendar -Property @{
        ShowTodayCircle   = $false
        MaxSelectionCount = 1
        }
    $form.Controls.Add($calendar)

    $okButton = New-Object Windows.Forms.Button -Property @{
    Location     = New-Object Drawing.Point 38, 165
    Size         = New-Object Drawing.Size 75, 23
    Text         = 'OK'
    DialogResult = [Windows.Forms.DialogResult]::OK
    }
$form.AcceptButton = $okButton
$form.Controls.Add($okButton)

    $cancelButton = New-Object Windows.Forms.Button -Property @{
    Location     = New-Object Drawing.Point 113, 165
    Size         = New-Object Drawing.Size 75, 23
    Text         = 'Cancelar'
    DialogResult = [Windows.Forms.DialogResult]::Cancel
    }
$form.CancelButton = $cancelButton
$form.Controls.Add($cancelButton)

$result = $form.ShowDialog()

if ($result -eq [Windows.Forms.DialogResult]::OK) {
    $Inicio = $calendar.SelectionStart
    Write-Host `n"Fecha Inicial de Auditoria: $($Inicio.ToShortDateString())"
}


<#----------------Selector fecha final-------------------#>

    $form = New-Object Windows.Forms.Form -Property @{
        StartPosition = [Windows.Forms.FormStartPosition]::CenterScreen
        Size          = New-Object Drawing.Size 243, 230
        Text          = 'Seleccione Fecha FINAL de busqueda'
        Topmost       = $true
        }

    $calendar = New-Object Windows.Forms.MonthCalendar -Property @{
        ShowTodayCircle   = $false
        MaxSelectionCount = 1
        }
    $form.Controls.Add($calendar)

    $okButton = New-Object Windows.Forms.Button -Property @{
    Location     = New-Object Drawing.Point 38, 165
    Size         = New-Object Drawing.Size 75, 23
    Text         = 'OK'
    DialogResult = [Windows.Forms.DialogResult]::OK
    }
$form.AcceptButton = $okButton
$form.Controls.Add($okButton)

    $cancelButton = New-Object Windows.Forms.Button -Property @{
    Location     = New-Object Drawing.Point 113, 165
    Size         = New-Object Drawing.Size 75, 23
    Text         = 'Cancelar'
    DialogResult = [Windows.Forms.DialogResult]::Cancel
    }
$form.CancelButton = $cancelButton
$form.Controls.Add($cancelButton)

$result = $form.ShowDialog()

if ($result -eq [Windows.Forms.DialogResult]::OK) {
    $Final = $calendar.SelectionStart
    Write-Host `n"Fecha Final de auditoría: $($Final.ToShortDateString())"
}

<# FIN DE SELECTOR DE FECHAS
---------------------------------------------------------------------
 SELECTOR DE EVENTOS LOGON/LOGOFF
#>

Write-Host `n"AQUI ESTAN LOS USUARIOS QUE SE LOGUEARON A ESTE EQUIPO:"

$ResolveEventType = @{ 7001 = 'Logon'; 7002 = 'Logoff' }
$FilterHashTable  =
    @{
        LogName      = 'system'
        ProviderName = 'Microsoft-Windows-Winlogon'
        ID           = 7001,7002
        StartTime    = $Inicio
        EndTime = $Final
    }

[Array]$WinEvents = 
Get-WinEvent -FilterHashtable $FilterHashTable | 
Select-Object @{ Name = 'Time'; Expression = { $_.TimeCreated } },
    @{ Name = 'Event'; Expression = { $ResolveEventType[ $_.ID ] } },
    @{ Name = 'User'; Expression = { $_.Properties[1].Value.Translate( [NTAccount] ) } }

$WinEvents | 
Where-Object{ $_.UserName -notlike "*-organization" } | 
Format-Table -AutoSize

<#----------------------------------------------
MOSTRAR DISCOS FISICOS
------------------------------------------------#>

Write-Host `n"ESTA ES LA INFORMACION DE LOS DISCOS FÍSICOS EN ESTE EQUIPO:"


$compu= "localhost"

Get-WmiObject Win32_LogicalDisk -computerName $compu | select Name, ProviderName,FileSystem,@{Name="Total Size";Expression={[int](
$_.size / 1GB)}}, @{Name="Free Space";Expression={[int]($_.freespace / 1GB)}} | ft -AutoSize
